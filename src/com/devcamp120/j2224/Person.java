package com.devcamp120.j2224;

import java.util.ArrayList;
import java.util.Arrays;

public class Person extends Animal {


    public Person(){
        System.out.println("ko có tham số");
    }

    public Person(int age , String name ){
        this.age = age ;
        this.name = name ;
    }
    public Person(String name , Boolean boo ){
        this.name = name ;
        this.boo = boo ;
    }
    public Person(String name , int age ,Boolean boo ,  ArrayList<Pet> pets){
        this.name = name ;
        this.boo = boo ;
        this.age = age;
        this.pets = pets ;
    }

    private int age;
    private String name ;
    private Boolean boo = true ;
    private ArrayList <Pet> pets ;

    @Override
    public String toString(){
        return "CPerson {\"name\" : " + this.name+ ", age : " + age + " , boolean : "+ this.boo + ", pets : "
        + this.pets + "}";
        
    }

    @Override
    public void animalSound() {
        // TODO Auto-generated method stub
        
    }


}
