package com.devcamp120.j2224;

import java.util.ArrayList;

import java.util.Iterator;

public class CIntegerArrayList implements ISumable{
   private ArrayList<Integer> mIntegerArrayList ;

    // constuctor 
   public CIntegerArrayList(ArrayList<Integer> mIntegerArrayList){
        this.mIntegerArrayList = mIntegerArrayList ;
   }
   
   // 2 phương thức (method) getter và setter cho mIntegerArrayList
   // ở trên private tạo cái gì ở dưới getter và setter fai tạo y chang 
  public ArrayList<Integer> getIntegerArrayList() {
      return mIntegerArrayList;
  }

  public void setIntegerArrayList(ArrayList<Integer> mIntegerArrayList) {
      this.mIntegerArrayList = mIntegerArrayList;
  }

  	/*  Iterator Duyệt các phần tử từ đầu đến cuối của một collection.
	 	Iterator cho phép xóa phần tử khi lặp một collection.

		hasNext(): có phần tử tiếp theo hay không

		next(): lấy phần tử tiếp theo
	
		remove(): loại bò phần tử cuối cùng
    */
  @Override
  public String getSum(){
    int sum = 0 ;
    for (Iterator iterator = this.mIntegerArrayList.iterator(); iterator.hasNext();) {
        // ép kiểu
        Integer integer = (Integer) iterator.next();
        sum += integer.intValue();
    }
    return "Đây là Sum của class CIntegerArrayList: " + sum;
  }

  public static void testSumable (ISumable arrayList){
    System.out.println(arrayList.getSum());
  }
}
