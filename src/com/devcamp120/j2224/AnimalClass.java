package com.devcamp120.j2224;

public enum AnimalClass {
    fish, amphibians, reptiles, mammals, birds
}
