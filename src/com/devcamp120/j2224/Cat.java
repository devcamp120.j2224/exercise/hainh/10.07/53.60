package com.devcamp120.j2224;

public class Cat extends Pet implements Irunable , Other {
    @Override
    public void running (){
        System.out.println("cat running");
    }

    @Override
    public void eating() {
        System.out.println("cat eating");
    }

    // implement từ class other , class other có 3 method , 3 tham số method khác nhau phải tạo giống
    @Override
    public void other() {
       System.out.println("cat bark");
    }

    @Override
    public int other(int param) {
        
        return 10;
    }

    @Override
    public String other(String param) {
        
        return "cat eating";
    }
    @Override
    public void animalSound(){
        System.out.println("cat mew mew");
    }
    @Override
    public void An(){
        System.out.println("cat eating");
    }
    public String toString(){
        return "tao là Mèo";
    }
}
