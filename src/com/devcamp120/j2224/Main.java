package com.devcamp120.j2224;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Pet pet1 = new Dog();
        pet1.An();
        pet1.animalSound();
        Pet pet2 = new Cat();
        
        //pet1 = new Cat();

        //pet1.An();
        //pet1.animalSound();
        ArrayList<Pet> petsList = new ArrayList<Pet>();

        petsList.add(pet2);
		// ép kiểu 1 cái 
		petsList.add((Pet) pet1);
        
        Person person1 = new Person() ; 
        Person person2 = new Person("Hải" , false) ; 
        Person person3 = new Person("Nguyễn Hoàng Hải" , 32 , false , petsList) ; 

        ArrayList<Person> arraylist = new ArrayList<Person>();
        //ArrayList<Pet> pet = new ArrayList<Pet>();

        // mượn class cha tạo ra class con ĐA HÌNH
       
        // add person vào arraylist có thể làm y chang với class animal
        arraylist.add(person1);
        arraylist.add(person2);
        arraylist.add(person3);

        for (Person person : arraylist){
            System.out.println(person.toString());
        }

        ArrayList<Integer> mIntegerArrayList = new ArrayList<>();
        // ép kiểu , version cũ , có dấu gạch nhưng vẫn xài dc , s là String 

		Integer inte1 = new Integer(10);  
		Integer inte2 = new Integer("20");  

        // ép kiểu , version mới , i ở đây là int
		Integer inte3 = Integer.valueOf("10");
		Integer inte4 = Integer.valueOf(15);
		mIntegerArrayList.add(inte1);
		mIntegerArrayList.add(inte2);
		mIntegerArrayList.add(inte3);
		mIntegerArrayList.add(inte4);

        // tạo new 1 class CIntegerArrayList chấm với hàm testSumable đã tính toán bên class CIntegerArrayList
        // nó sẽ + tất cả giá trị , ko cần biết là String hay int 
        
		CIntegerArrayList intergeArrListObject = new CIntegerArrayList(mIntegerArrayList);
        
	    CIntegerArrayList.testSumable(intergeArrListObject);

		// còn 1 hàm làm cho aaray , tham khảo bài 53.10

    }
}
