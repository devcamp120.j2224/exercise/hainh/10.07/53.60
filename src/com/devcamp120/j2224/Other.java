package com.devcamp120.j2224;

public interface Other {
    void other();
    int other(int param);
    String other(String param);
}
